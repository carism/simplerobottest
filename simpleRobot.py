import pdb
import math
import hu
import itertools
from collections import deque
import numpy as np

import transformations as transf
from objects import compileChainFramesOS
import shapes
import cspace
import objects
from objects import *
import planGlobals as glob
from planGlobals import assertHPN
from traceFile import tr, debug
from conf import RobotJointConf, RobotCartConf
from genericRobot import GenericRobot

Ident = hu.Transform(np.eye(4, dtype=np.float64)) # identity transform

################################################################
# Define the robot, ideally would be from a URDF.
################################################################

# 'beige' for movies, but that doesn't show up against white background of 2D
# Python simulator.
robotColor = 'gold'

# Shorthand for defining an axis aligned Box and a composite Shape.
def Ba(bb, **prop): return shapes.BoxAligned(np.array(bb), Ident, **prop)
def Sh(*args, **prop): return shapes.Shape(list(args), Ident, **prop)

# Some parameters for defining some of the shapes, notably the hand and the base.
params = {'fingerLength' : 0.06,
          'fingerWidth' :  0.04,
          'fingerThick' :  0.02,
          'palmLength' : 0.09,
          'palmWidth' : 0.175,
          'palmThick' : 0.05,
          'gripperOffset': 0.04,
          'gripMax' :      0.081,
          'zRange': (0.0, 1.5),
          'zRangeHand': (0.35, 2.0),
          'robotBaseZ': 0.5,
          'robotBaseDX': 0.3,           # length = 2*dx
          'robotBaseDY': 0.3,           # width = 2*dy
          'armExtendMax': 0.5,
          'armLiftDZ':0.2
          }

# The "link" for the robot base
robotBaseLink = Sh(\
    Ba([(-params['robotBaseDX'], -params['robotBaseDY'], 0.0),
        ( params['robotBaseDX'],  params['robotBaseDY'], params['robotBaseZ'])],
       name='base')
    )

# The "links" for the  head.  This is a slightly simplified PR2 head.
robotHeadLinks = [\
    None, None, None,
    Sh(Ba([(-0.1, -0.025, -0.05), (0.1, 0.025, 0.05)], name='kinect'))
    # a "beam" from the center of the kinect, along Z axis (for debugging)
    #Ba([(-0.01, -0.01, 0), (0.01, 0.01, 2)], name='sensorBeam')
    ]

# The joints for the pan/tilt head (similar to PR2)
def robotHeadJoints():
    dz = params['robotBaseZ']
    dx = params['robotBaseDX']
    rot = transf.euler_matrix(-1.57079632679, 0, -1.57079632679, axes='sxyz')
    return [Revolute('head_pan_joint',
                     hu.Transform(transf.translation_matrix([dx*0.75,0.0,dz])),
                     (-math.pi/2, math.pi/2.), (0.,0.,1.)),
            Revolute('head_tilt_joint',
                     Ident,
                      (-0.4, 1.4), (0.,1.,0.)),
            Rigid('kinect_offset',
                  hu.Transform(transf.translation_matrix([-0.05,0.0,0.25])),
                  None, None),
            Rigid('kinect_ir_optical_frame_joint',
                  hu.Transform(rot),
                  None, None)]

# The links for the robot (just a beam)
def robotArmLinks():
    links = [\
        None,                           # offset
        None,
        Sh(Ba([(-0.5, -0.05, -0.05), (0.0, 0.05, 0.05)], name='arm')),
        ]
    return links

# The joints for the robot, two perpendicular prismatic joints (a lift
# along z and extend along x).
def robotArmJoints():
    dz = params['robotBaseZ']
    dx = params['robotBaseDX']
    dlift = params['armLiftDZ']
    return [Rigid('arm_fixed_offset',
                  hu.Transform(transf.translation_matrix([dx,0.0,0.5*dz])),
                  None, None),
            Prismatic('arm_lift_joint',
                      Ident,
                      (-dlift, dlift), (0.,0.,1.)),
            Prismatic('arm_extend_joint',
                      Ident,
                      (0.0, params['armExtendMax']), (1.,0.,0))]

# The links for the parallel jaw gripper
def robotGripperLinks():
    palm_dx = params['palmLength']
    palm_dy = params['palmWidth']
    palm_dz = params['palmThick']
    fing_dx = params['fingerLength']
    fing_dy = params['fingerWidth']
    fing_dz = params['fingerThick']
    return [Sh(shapes.Box(palm_dx, palm_dy, palm_dz, Ident, name='palm')),
            Sh(shapes.Box(fing_dx, fing_dy, fing_dz, Ident, name='finger1')),
            Sh(shapes.Box(fing_dx, fing_dy, fing_dz, Ident, name='finger2'))]

# The joints for the gripper
def robotGripperJoints():
    o = params['gripperOffset']
    palm_dx = params['palmLength']
    fing_dx = params['fingerLength']
    fing_dy = params['fingerWidth']
    fing_dz = params['fingerThick']
    return [Rigid('palm',
                  hu.Transform(transf.translation_matrix([o + palm_dx/2.,0.0,0.0])),
                  None, None),
            Prismatic('finger1',
                      hu.Transform(transf.translation_matrix([palm_dx/2+fing_dx/2.,fing_dy/2.,0.0])),
                      (0.0, params['gripMax']/2.), (0.,1.,0)),
            Prismatic('finger2',
                      hu.Transform(transf.translation_matrix([0.0,-fing_dy,0.0])),
                      (0.0, 0.081), (0.,-1.,0))]

# Some transforms that relate "tool" to wrist.

# Transforms from wrist frame to hand frame
# This leaves X axis unchanged
gripperToolOffsetX = hu.Pose(0.18,0.0,0.0,0.0)

# Displace wrist to grasp face frame, for this arm, the face frame Z
# will point along -Y.
gFaceFrame = hu.Transform(np.array([(0.,1.,0.,0.18),
                                    (0.,0.,1.,0.),
                                    (1.,0.,0.,0.),
                                    (0.,0.,0.,1.)]))
gripperFaceFrame = {'right':gFaceFrame}

################################################################
# This is used to make the robot instances
################################################################

robotChains = {}
def makeRobotChains(name, workspaceBounds, new=True):
    global robotChains
    if not new and name in robotChains:
         return robotChains[name]
    # Chain for base
    baseChain = Planar('robotBase', 'root', robotBaseLink, workspaceBounds)
    # Chain for left arm
    armChain = Chain('robotArm', 'robotBase_theta',
                     robotArmJoints(), robotArmLinks())
    # Chain for gripper
    gripperChain = GripperChain('robotGripper', 'arm_extend_joint',
                                robotGripperJoints(), robotGripperLinks())
    # Chain for head
    headChain = Chain('robotHead', 'robotBase_theta',
                      robotHeadJoints(), robotHeadLinks)
    robotChains[name] = MultiChain(name,
                                   [baseChain, armChain, gripperChain, headChain])
    return robotChains[name]

# In case we want to make the planning robot slightly "fatter".
# The radius is baseCovariance radius, angle in baseCovariance angle,
# reachPct is percentage of maximum reach of arm.
def makeRobotChainsShadow(name, workspaceBounds, radiusVar=0.0, angleVar=0.0, reachPct=1.0):
    # Just the normal chains for now
    return makeRobotChains(name, workspaceBounds)

################################################################
# Conf and CartConf, mostly generic
################################################################

# This behaves like a dictionary, except that it doesn't support side effects.
class JointConf(RobotJointConf):
    def __init__(self, conf, robot):
        RobotJointConf.__init__(self, conf, robot)
    def copy(self):
        return JointConf(self.conf.copy(), self.robot)
    def handWorkspace(self):
        z0 = params['robotBaseZ']*0.5                    # mid
        x0 = params['robotBaseDX']
        xmax = params['armExtendMax']
        dlift = params['armLiftDZ']
        bb = ((x0, -0.001, z0-dlift),(x0+xmax, 0.001, z0+dlift))
        return shapes.BoxAligned(np.array(bb), Ident).applyTrans(self.basePose())

        assert None, 'Not implemented'

# In a cartesian configuration, we specify frames for base, hand and head
class CartConf(RobotCartConf):
    def __init__(self, conf, robot):
        RobotCartConf.__init__(self, conf, robot)
    def copy(self):
        return CartConf(self.conf.copy(), self.robot)

# This specified a joint configuartion, specifying the joint angles for all the chains.
robotInit = {'robotBase':[0.0,0.0,0.0],
             'robotArm': [0.0, 0.0],
             'robotGripper': [0.02],
             'robotHead': [0.0, 0.0]}

def ground(pose):
    params = list(pose.pose().xyztTuple())
    params[2] = 0.0
    return hu.Pose(*params)

################################################################
# The actual robot class, a subclass of GenericRobot
################################################################

# Does not include the torso
robotChainDependencies = \
                     {'robotBase' : ['robotBase', 'robotHead', 'robotArm', 'robotGripper'],
                      'robotHead' : ['robotHead'],
                      'robotArm' : ['robotArm', 'robotGripper'],
                      'robotGripper' : []
                      }

robotChainDependenciesRev = \
                        {'robotBase' : [],
                         'robotHead' : ['robotBase'],
                         'robotArm' : ['robotBase'],
                         'robotGripper' : ['robotArm', 'robotBase'],
                      }

# This basically implements a Chain type interface
class SimpleRobot(GenericRobot):
    def __init__(self, name, chains, color = robotColor):
        self.chains = chains
        self.color = color
        self.name = name
        # These are (focal, height, width, length, n)
        self.scanner = (0.3, 0.2, 0.2, 5, 30) # Kinect
        # These names encode the "order of actuation" used in interpolation
        self.chainNames = ['robotGripper', 'robotBase', 'robotArm', 'robotHead']
        self.bodyChains = ['robotBase', 'robotHead']
        # This is overriden by testRig.py
        self.moveChainNames = ['robotBase', 'robotArm']
        self.armChainNames = {'right':'robotArm'}
        self.gripperChainNames = {'right':'robotGripper'}
        self.wristFrameNames = {'right':'arm_extend_joint'}
        self.baseChainName = 'robotBase'
        self.headChainName = 'robotHead'
        # No self-collision for this robot...  This is a bit hacky
        self.selfCollideChainNames = [[], [], [], []]
        # This has the X axis pointing along fingers
        self.toolOffsetX = {'right': gripperToolOffsetX}
        self.gripperFaceFrame = gripperFaceFrame
        self.gripMax = params['gripMax']

        self.nominalConf = None
        self.confCache = {}
        self.confCacheKeys = deque([])  # in order of arrival

        self.compiledChainsOS = compileChainFramesOS(self) # generic
        self.chainDependencies = robotChainDependencies    # see above
        self.chainDependRev = robotChainDependenciesRev    # see above

    def handNames(self):
        return ['right']

    def makeJointConf(self, confDict):
        return JointConf(confDict, self)

    def makeCartConf(self, conf):
        return CartConf(conf, self)

    def clearInvKinCache(self):         # in case there is one
        pass

    # Base transforms that can achieve the specified wrist pose.  This
    # is crucial for grasping.
    def potentialBasePosesGen(self, wrist, hand, n=None, complain=True):
        n = n or 5
        extend = float(params['armExtendMax'])/(n-1)
        results = []
        for i in range(n):
            e = i*extend + params['robotBaseDX']
            results.append(ground(wrist.compose(hu.Pose(-e, 0., 0., 0.))))
        return results

    def fingerSupportFrame(self, hand, width):
        # The old way...
        # Origin is on the inside surface of the finger (at the far tip).
        # The -0.18 is from finger tip to the wrist  -- if using wrist frame
        # mat = np.dot(transf.euler_matrix(-math.pi/2, math.pi/2, 0.0, 'ryxz'),
        #              transf.translation_matrix([0.0, -0.18, -width/2]))

        # y points along finger approach, z points in closing direction
        # offset aligns with the grasp face.
        # This is global gripperFaceFrame offset to center object
        gripperFaceFrame_dy = hu.Transform(np.array([(0.,1.,0.,0.18),
                                                       (0.,0.,1.,-width/2),
                                                       (1.,0.,0.,0.),
                                                       (0.,0.,0.,1.)]))
        return gripperFaceFrame_dy

    # This is useful to get the base shape when we don't yet have a conf
    def baseLinkShape(self, basePose=None):
        if basePose:
            return robotBaseLink.applyTrans(basePose)
        else:
            return robotBaseLink

    # This is a "partial" inverse kinematics involving only one hand
    # and (optionally) a basePose and/or grip.  The defaultConf
    # specifies the other parameters.  In the missing basePose case,
    # it should generate alternatives.
    def inverseKinWristGen(self, wrist, hand, defaultConf,
                           basePose=None, grip=None,
                           complain=True, n=None, counts=None):
        if basePose:                    # base specicied
            conf = self.confFromBaseAndWrist(basePose, hand, wrist,
                                             defaultConf, counts=None, grip=grip)
            if conf and all(conf.conf.values()): yield conf
            return                      # there is only one...
        else:
            for basePose in \
                    self.potentialBasePosesGen(wrist, hand,
                                               n=n, complain=complain):
                conf = self.confFromBaseAndWrist(basePose, hand, wrist,
                                                 defaultConf, counts=counts, grip=grip)
                if conf and all(conf.conf.values()): yield conf

    # Uses inverse kinematics
    def confFromBaseAndWrist(self, basePose, hand, wrist,
                             defaultConf, counts=None, grip=None):
        robot = defaultConf.robot
        # Get a grip...
        grip = grip or defaultConf[robot.gripperChainNames[hand]]
        grip = min(params['gripMax']-0.001, grip) # don't exceed the max
        # Make CartConf
        cart = CartConf({'robotBaseFrame': basePose}, self)
        cart.conf['robotArmFrame'] = wrist 
        cart.conf['robotGripper'] = [grip] # !! pick better value
        # Check inverse kinematics
        conf = self.inverseKin(cart, defaultConf)
        if None in conf.values():
            tr('potentialGraspConfsLose', 'invkin failure')
            if counts: counts[0] += 1       # kin failure
            return
        return conf

    # Note that the "ArmFrame" is the wrist frame.
    def forwardKin(self, conf, complain = False, fail = False):
        shapes, frames = self.placement(conf, None, getShapes=[])
        return CartConf(\
            {'robotBaseFrame': frames[self.chains.chainsByName['robotBase'].joints[-1].name],
             'robotArmFrame':
             frames[self.chains.chainsByName['robotArm'].joints[-1].name],
             'robotHeadFrame': frames[self.chains.chainsByName['robotHead'].joints[-1].name]},
            self)

    # Interface to inverse kinematics
    def inverseKin(self, cart, conf, returnAll = False,
                   complain = False, fail = False):
        """Map from cartesian configuration (wrists and head) to joint
        configuration."""
        assertHPN( conf or self.nominalConf, 'Illegal args to invKin')
        conf = conf or self.nominalConf
        # Base
        if 'robotBaseFrame' in cart.conf:
            baseTrans = cart.get('robotBase', None)
            baseChain = self.chains.chainsByName['robotBase']
            baseAngles = baseChain.inverseKin(Ident, baseTrans)
            if not baseAngles:
                if complain: print 'Failed invkin for base'
                if fail: raise Exception, 'Failed invkin for base'
                conf = conf.set('robotBase', None) # explicitly show failure
            else:
                conf = conf.set('robotBase', list(baseAngles))
        else:
            # Keep what is in conf
            baseTrans = conf.basePose()
        if 'robotArmFrame' in cart.conf:
            # Solve for arm
            armChain = self.chains.chainsByName['robotArm']
            limits = armChain.limits()
            armTrans = self.chains.chainsByName['robotArm'].forwardKin(baseTrans, [0., 0.])
            offset = armTrans.inverse().compose(cart['robotArm'])
            pose = offset.pose(fail=False)
            # We should just have an offset along x within the legal limit
            if (not pose) or \
                   not(limits[0][0] <= pose.z <= limits[0][1]) or \
                   not(limits[1][0] <= pose.x <= limits[1][1]) or \
                   abs(pose.theta) > 0.001 or \
                   abs(pose.y) > 0.001:
                if complain:
                    raw_input('Failed invkin for arm')
                if fail: raise Exception, 'Failed invkin for arm'
                conf = conf.set('robotArm', None)
            else:
                conf = conf.set('robotArm', [pose.z, pose.x])
        if 'robotHeadFrame' in cart.conf:
            headAngles = headInvKin(self.chains,
                                    baseTrans, cart['robotHead'])
            if not headAngles:
                if complain: print 'Failed invkin for head'
                if fail: raise Exception, 'Failed invkin for head'
                conf = conf.set('robotHead', None)
            else:
                conf = conf.set('robotHead', headAngles)
        if 'robotGripper' in cart.conf:
            g = cart.conf['robotGripper']
            conf = conf.set('robotGripper', g if isinstance(g, (list,tuple)) else [g])

        # print 'from invkin', conf.conf
        return conf

########################################
# Inverse kinematics support functions, for head...
########################################

headInvKinCacheStats = [0,0]
headInvKinCache = {}

def headInvKin(chains, torso, targetFrame,
                 collisionAware=False, allowedViewError = 1e-3):
    headChain = chains.chainsByName['robotHead']
    limits = headChain.limits()
    # Displacement from movable joints to sensor
    headSensorOffsetY = reduce(np.dot, [j.trans for j in headChain.joints[2:-1]]).matrix
    headSensorOffsetZ = reduce(np.dot, [j.trans for j in headChain.joints[1:-1]]).matrix

    headRotationFrameZ = np.dot(torso, headChain.joints[0].trans)
    # Target point relative to torso
    relFramePoint = torso.inverse().compose(targetFrame).point()

    if debug('headInvKin'): print 'relFramePoint', relFramePoint

    key = relFramePoint
    headInvKinCacheStats[0] += 1
    if key in headInvKinCache:
        headInvKinCacheStats[1] += 1
        return headInvKinCache[key]

    if abs(relFramePoint.matrix[0,0]) < 0.1:
        # If the frame is just the head frame, then displace it.
        targetFrame = targetFrame.compose(hu.Pose(0.,0., 0.2, 0.0))

    targetZ = headRotationFrameZ.inverse().applyToPoint(targetFrame.point()).matrix
    angles1 = tangentSol(targetZ[0,0], targetZ[1,0], headSensorOffsetZ[0,3], headSensorOffsetZ[1,3])    
    angles1 += list(limits[0])
    angles1 = set(angles1)

    if debug('headInvKin'): print 'angles1', angles1
    
    best = None
    bestScore = 1.0e10
    bestError = None
    # print 'zero\n', headChain.forwardKin(torso, (0, 0)).matrix
    for a1 in angles1:
        headRotationFrameZ = np.dot(torso, headChain.joints[0].transform(a1))
        headRotationFrameY = np.dot(headRotationFrameZ, headChain.joints[1].trans)
        targetY = headRotationFrameY.inverse().applyToPoint(targetFrame.point()).matrix
        angles2 = [-x for x in tangentSol(targetY[0,0], targetY[2,0],
                                          headSensorOffsetY[0,3], headSensorOffsetY[2,3])]
        angles2 += list(limits[1])
        angles2 = set(angles2)

        if debug('headInvKin'): print 'angles2', angles2

        for a2 in angles2:
            if headChain.valid([a1, a2]):
                headTrans = headChain.forwardKin(torso, (a1, a2))
                sensorCoords = headTrans.inverse().applyToPoint(targetFrame.point()).matrix
                if sensorCoords[2] < 0.: continue
                score = math.sqrt(sensorCoords[0]**2 + sensorCoords[1]**2)

                if debug('headInvKin'): print 'score', score, 'sensorCoords', sensorCoords

                if score < bestScore:
                    best = [a1, a2]
                    bestScore = score
                    bestError = sensorCoords

    ans = best if bestScore <= allowedViewError else None
    headInvKinCache[key] = ans
    return ans

def tangentSolOld(x, y, x0, y0):
    # print 'X', (x,y), 'X0', (x0, y0)
    alpha = math.atan2(y,x)
    theta0 = math.atan2(y0,x0)
    r = math.sqrt(x0*x0 + y0*y0)
    d = math.sqrt(x*x + y*y)
    theta1 = math.pi/2
    ac = math.acos((r/d)*math.cos(theta0-theta1))
    # print 'alpha', alpha, 'theta0', theta0, 'ac', ac
    values =  [alpha + theta1 + ac,
               alpha + theta1 - ac,
               alpha - theta1 + ac,
               alpha - theta1 - ac]
    keep = []
    for angle in values:
        v = (x - r*math.cos(angle + theta0),
             y - r*math.sin(angle + theta0))
        vangle = math.atan2(v[1], v[0])
        # print 'angle', angle, 'atan', vangle
        if hu.angleDiff(angle, vangle) < 0.001:
            keep.append(vangle)
    # This generally returns two answers, but they may not be within limits.
    # print 'keep', keep
    return keep

# x0,y0 is sensor point (rotation is at origin)
# x,y is target point
# The sensor is pointing along the x axis when angle is zero!
# At desired sensor location we have a triangle (origin, sensor, target)
# l is distance from sensor to target, found from law of cosines.
# alpha is angle target-origin-sensor, also from law of cosines
def tangentSol(x, y, x0, y0):
    def quad(a,b,c):
        disc = b*b - 4*a*c
        if disc < 0: return []
        discr = disc**0.5
        return [(-b + discr)/(2*a), (-b - discr)/(2*a)]
        
    ph= math.atan2(y,x)
    d = math.sqrt(x*x + y*y)
    th0 = math.atan2(y0,x0)
    r = math.sqrt(x0*x0 + y0*y0)
    # c1 is cos of angle between sensor direction and line to x0,y0
    c1 = math.cos(math.pi - th0)
    # vals for l
    lvals = quad(1.0, -2*r*c1, r*r-d*d)
    # print 'lvals', lvals
    if not lvals: return []
    # vals for alpha
    avals = [math.acos(max(-1.0, min(1.0, (l*l - r*r - d*d)/(-2*r*d)))) for l in lvals]
    # print 'avals', avals
    # angs are candidate rotation angles
    angs = []
    for alpha in avals:
        angs.extend([ph - alpha - th0, ph + alpha - th0])
    angs = [hu.fixAnglePlusMinusPi(a) for a in angs]
    # print 'angs', angs
    ans = []
    for ang in angs:
        # check each angle, (x1,y1) is sensor location
        x1 = r*math.cos(th0 + ang)
        y1 = r*math.sin(th0 + ang)
        # distance sensor-target
        l = math.sqrt((x1-x)**2 + (y1-y)**2)
        # the sensor direction rotates by ang, check that it points at target
        ex = abs(x - (x1 + l*math.cos(ang)))
        ey = abs(y - (y1 + l*math.sin(ang)))
        # print 'ang', ang, 'ex', ex, 'ey', ey
        # keep the ones with low error
        if ex < 0.001 and ey < 0.001:
            ans.append(ang)
    return ans

################################################################
# Interface for testRig to create robot instances
################################################################

def makeRobot(workspace, radiusVar=0.0):
    robot = SimpleRobot('R', makeRobotChainsShadow('R', workspace, radiusVar=radiusVar))
    # This affects randomConf and stepAlongLine, unless overriden
    robot.moveChainNames = ['robotBase', 'robotArm']
    # This uses the initial Conf as a reference for inverseKin, when no conf is given
    robot.nominalConf = JointConf(robotInit, robot)
    return robot

standardConf = None
def makeConf(robot, x, y, th=0.0, g=0.07, vertical=None):
    global standardConf
    dx = dy = dz = 0
    dt = 0.0
    if standardConf:
        c = standardConf.copy()
        c.conf['robotBase'] = [x, y, th]
        c.conf['robotGripper'] = [g]
        return c
    else:
        c = JointConf(robotInit.copy(), robot)
        c = c.set('robotBase', [x, y, th])
        c = c.set('robotGripper', [g])
        cart = c.cartConf()
        base = cart['robotBase']
        h = hu.Pose(0.5, 0., 0.45, 0.)    # displacement of hand
        cart = cart.set('robotArm', base.compose(h))
        cart = cart.set('robotHead', base.compose(h))
        c = robot.inverseKin(cart, c)
        c.conf['robotHead'] = [0., 0.]
        assert all(c.values())
        standardConf = c
        return c

print 'Loaded simpleRobot.py'
