# This appends the redux package to the PYTHONPATH
import sys

# This imports all of the generic bhpn functions
import time
from bhpn_core.fbch import HPN, State
from bhpn_core.belief import Bd
import mm_operators.pr2Ops as ops
from mm_operators.pr2Fluents import In 
from state_estimator.pr2PlanBel import BeliefContext 
from state_estimator.pr2BeliefState import BeliefState 
from simpleRobotPrims import RealWorld
from simpleRobot import makeConf, makeRobot
from pr2Util import DomainProbs
from objects import World, WorldState
from pr2RoadMap import RoadMap
from planUtil import ObjPlaceB, PoseD
from hu import Pose, Transform
import numpy as np
from shapes import BoxAligned, Shape
from dist import DDist

# functions for HPN sim
import windowManager3D as wm

#######################################################################
# SHAPE AND BOX ALIGNED DEFINITIONS
def Ba(bb, **prop): return BoxAligned(np.array(bb), None, **prop)
def Sh(args, **prop): return Shape(list(args), None, **prop)

#######################################################################
# DOMAIN PROBABILITIES
typicalErrProbs = DomainProbs(
            # stdev, constant, assuming we control it by tracking while moving
            odoError = (0.01, 0.01, 1e-5, 0.03),
            # odoError = (0.02, 0.02, 1e-5, 0.05), okay
            # odoError = (0.03, 0.03, 1e-5, 0.1),
            # variance in observations; diagonal for now
            obsVar = (0.005**2, 0.005**2, 1e-5 **2, 0.01**2),
            # big angle var from robot experience
            # obsVar = (0.005**2, 0.005**2,0.005**2, 0.15**2),
            # get type of object wrong
            obsTypeErrProb = 0.05,
            # fail to pick or place in the way characterized by the Gaussian
            pickFailProb = 0.02,
            placeFailProb = 0.02,
            pushFailProb = 0.1,
            # variance in grasp after picking
            pickVar = (0.001**2, 0.001**2, 1e-11, 0.002**2),
            # variance in pose after placing
            placeVar = (0.005**2, 0.005**2, 1e-11, 0.01**2),
            pushVar = (0.01**2, 0.01**2, 1e-11, 0.02**2),
            # pickTolerance
            pickTolerance = (0.03, 0.03, 0.03, 0.1),
            maxGraspVar = (0.0051**2, .0051**2, .005**2, .015**2),
            maxPushVar = (0.01**2, .01**2, .0001**2, .1**2),
            # This can't be super small in Z, because it is eventually
            # applied to cart confs of end effectors.
            moveConfDelta = (0.001, 0.001, 1.0e-4, 0.002),
            shadowDelta = (0.001, 0.001, 1.0e-3, 0.002),
            # Use this for placing objects
            # Most recent
            placeDelta = (0.01, 0.01, 1.0e-3, 0.02),
            graspDelta = (0.005, 0.005, 1.0e-3, 0.008))

#######################################################################
# OPERATORS
# Select which operators should be used from:
# move, lookAt, moveNB, achCanReach, achCanReachNB, achCanPickPlace, 
# poseAchIn, bLoc1, bLoc2, condPose, push, achCanPush, pick, place
operators = [ops.move, ops.lookAt, ops.bLoc1, ops.bLoc2, ops.condPose]

print 'This test will use the following operators: '

for op in operators:
	print '     ' + op.name

########################################################################
# OBJECTS

## Don't get this part...
transObsCovar = typicalErrProbs.obsVar[0:2, 0:2]
rotObsCovar = typicalErrProbs.obsVar[3:4, 3:4]
##

frame = [Transform(np.array([(0.,1.,0.,0.),
                             (0.,0.,1.,-0.025),
                             (1.,0.,0.,0.02), 
                             (0.,0.,0.,1.)]))]
# Moveable Objects
objAName = 'objA'
objAPose = Pose(1.0, 2.0, 0.25, 0.3)
objAVar = (0.1, 0.1, 0.1, 0.1)
objAPoseD = PoseD(objAPose, objAVar)
objAFaceFrames = frame
objARestingFace = 0
objAPlaceB = ObjPlaceB(objAName, objAFaceFrames, objARestingFace, objAPoseD)

objA = {'name':           objAName,
        'shape':          Sh([Ba([(0.0, 0.0, 0.0), (0.5, 0.5, 1.0)])], name='objA'),
        'placeB':         objAPlaceB,
        'transObsCovar':  transObsCovar,
        'rotObsCovar':    rotObsCovar,
        'type':           'box'}

objBName = 'objB'
objBPose = Pose(0.5, 1.0, 0.25, 0.4)
objBVar = (0.01, 0.01, 0.01, 0.01)
objBPoseD = PoseD(objBPose, objBVar)
objBFaceFrames = frame
objBRestingFace = 0
objBPlaceB = ObjPlaceB(objBName, objBFaceFrames, objBRestingFace, objBPoseD)

objB = {'name':           objBName,
        'shape':          Sh([Ba([(0.0, 0.0, 0.0), (0.5, 0.5, 1.0)])], name='objB'),
        'placeB':         objBPlaceB,
        'transObsCovar':  transObsCovar,
        'rotObsCovar':    rotObsCovar,
        'type':           'box'}
# moveableObjects = [objA, objB]
# FOR NOW: All objects are fixed. Moveable objects need a grasp description
moveableObjects = []

# Fixed Objects
tableName = 'table'
tablePose = Pose(1.5, 2.5, 3.5, 0.3)
tableVar = (.01, .01, .01, .01)
tablePoseD = PoseD(tablePose, tableVar)
tableFaceFrames = frame
tableRestingFace = 0
tablePlaceB = ObjPlaceB(tableName, tableFaceFrames, tableRestingFace, tablePoseD)

table = {'name':            tableName,
         'shape':           Sh([Ba([(0.0, 0.0, 0.0), (1.5, 2.5, 1.0)])], name='table'),
         'placeB':          tablePlaceB,
         'transObsCovar':   transObsCovar,
         'rotObsCovar':     rotObsCovar,
         'type':            'table'}
#fixedObjects = [table, objA, objB]
fixedObjects = [objA, objB]
# Regions
tableLeftName = 'tableLeft'
tableLeft = {'name':    tableLeftName,
             'shape':   Sh([Ba([(0.0, 0.0, 0.0), (1.5, 2.5, 0.0001)])], name=tableLeftName),
             'pose':    Pose(1.1, 0.0, 0.1, 0.1),
             'objName': 'table'}
regions = [tableLeft]

########################################################################
# INITIALIZE WORLD AND ROBOT 
world = World()
#robot = makeRobot()  # should take in urdf
#name = 'simpleRobot'
workspace = np.array([(-1.0, -2.5, 0.0), (3.0, 2.5, 2.0)])
world.robot = makeRobot(workspace)
robotHome = makeConf(world.robot, 0.1, 0.2, 0.1) #(robot, x, y, theta)

# Add all objects and regions to the world
for obj in moveableObjects + fixedObjects:
    world.addObjectShape(obj['shape'])

#for region in regions:
#    world.addObjectRegion(region['objName'], region['name'], region['shape'], region['pose'])

########################################################################
# INITIALIZE BELIEF
roadMap = RoadMap(robotHome, world)
beliefContext = BeliefContext(world, roadMap, typicalErrProbs)

beliefState = BeliefState(beliefContext, robotHome, {}, {})

isFixed = False
for obj in moveableObjects:
    beliefState.addObj(obj['name'], obj['placeB'], isFixed, obj['transObsCovar'], obj['rotObsCovar'], obj['type'])

isFixed = True
for obj in fixedObjects:
    beliefState.addObj(obj['name'], obj['placeB'], isFixed, obj['transObsCovar'], obj['rotObsCovar'], obj['type'])

beliefState.makePBS()
state = State([], beliefState)

env = RealWorld(world, beliefState, typicalErrProbs, world.robot)

########################################################################
# PLANNING GOAL
goal = State([Bd([In(['objA', 'tableLeft']), True, 0.9], True)])

########################################################################
# START THE SIM
windowSizes = {'W': 600, 'Belief': 500, 'World': 500, 'MAP': 500}
((wx0, wy0, _), (wx1, wy1, wdz)) = workspace
viewPort = [wx0-0.1, wx1+0.1, wy0-0.1, wy1+0.1, 0.0, wdz]
wm.makeWindow('W', viewPort, windowSizes.get('W',600))
wm.makeWindow('Belief', viewPort, windowSizes.get('Belief',500))
wm.makeWindow('World', viewPort, windowSizes.get('World',500))

wm.getWindow('Belief').startCapture()
wm.getWindow('World').startCapture()

beliefState.pbs.draw(0.9, 'Belief')
beliefState.pbs.draw(0.9, 'W')
env.draw('World')
'''
worldState = WorldState(world)
worldState.draw('World')
for regName in beliefState.pbs.regions:
    env.regionShapes[regName].draw('World', 'purple')
    if beliefState.pbs.regions and debug('regions'):
        raw_input('Regions')
'''
time.sleep(10.0)

########################################################################
# BEGIN BHPN

HPN(state, goal, operators, env)